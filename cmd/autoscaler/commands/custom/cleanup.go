package custom

import (
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func NewCleanupCommand() cli.Command {
	cmd := new(CleanupCommand)
	cmd.abstractCustomCommand.customCommand = cmd

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:    "cleanup",
			Aliases: []string{"cl"},
			Usage:   "Cleanup the environment for Custom Executor",
			Description: `
This is the implementation of Cleanup stage of the Custom Executor.

This command is removing the Virtual Machine that was dedicated for
the job scripts execution.

Details about how this command is used can be found at
https://docs.gitlab.com/runner/executors/custom.html#cleanup.`,
		},
	}
}

type CleanupCommand struct {
	abstractCustomCommand

	cfg    config.Global
	logger logging.Logger

	vmName   string
	provider providers.Provider
}

func (c *CleanupCommand) CustomExecute(ctx *cli.Context) error {
	err := c.init(ctx)
	if err != nil {
		return fmt.Errorf("couldn't initialize CleanupCommand: %w", err)
	}

	c.logger.Info("Executing the command")

	vmInst, err := c.provider.Get(ctx.Ctx, c.vmName)
	if err != nil {
		return fmt.Errorf("couldn't get the VM %q details: %w", c.vmName, err)
	}

	fmt.Println("Deleting job's virtual machine...")

	err = c.provider.Delete(ctx.Ctx, vmInst)
	if err != nil {
		return fmt.Errorf("couldn't delete the VM %q: %w", c.vmName, err)
	}

	fmt.Println("Virtual machine deleted!")

	return nil
}

func (c *CleanupCommand) init(ctx *cli.Context) error {
	c.cfg = ctx.Config()
	c.logger = ctx.
		Logger().
		WithField("command", "cleanup_exec")

	var err error

	runnerData := vm.NameRunnerData{
		RunnerShortToken: runner.GetAdapter().ShortToken(),
		ProjectURL:       runner.GetAdapter().ProjectURL(),
		PipelineID:       runner.GetAdapter().PipelineID(),
		JobID:            runner.GetAdapter().JobID(),
	}

	c.vmName = vm.NewName(c.cfg.VMTag, runnerData)

	c.provider, err = providers.Factorize(c.cfg, c.logger)
	if err != nil {
		return fmt.Errorf("couldn't factorize provider: %w", err)
	}

	return nil
}
