package vm

import (
	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
)

type Instance struct {
	Name string

	IPAddress string

	// For WinRM executor
	Username string
	Password string

	GCP gcp.Instance
}
