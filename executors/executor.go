package executors

import (
	"context"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/factories"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const factoryType = "executor"

type Executor interface {
	Execute(ctx context.Context, vmInst vm.Instance, script []byte) error
	ProvisionScript() string
}

type Factory func(cfg config.Global, logger logging.Logger) (Executor, error)

var factoriesRegistry = factories.NewRegistry(factoryType)

func MustRegister(name string, f Factory) {
	factoriesRegistry.MustRegister(name, f)
}

func Factorize(cfg config.Global, logger logging.Logger) (Executor, error) {
	factory, err := factoriesRegistry.Get(cfg.Executor)
	if err != nil {
		return nil, err
	}

	return factory.(Factory)(cfg, logger)
}
