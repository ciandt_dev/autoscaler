package gcp

import (
	"bytes"
	"context"
	"errors"
	"fmt"

	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler"
	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/encoding"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const (
	Name = "gcp"

	startupScript        = "startup-script"
	startupScriptWindows = "windows-startup-script-ps1"

	operationStateDone = "DONE"

	windowsKeysMetadataKey   = "windows-keys"
	vmCredentialsMetadataKey = "autoscaler-vm-credentials"

	osWindows = "windows"

	serialConsolePassword  = 4
	serialConsoleReadiness = 1
)

type osProvisioner func(ctx context.Context, logger logging.Logger, vmInst *vm.Instance, instance *compute.Instance) error

type Provider struct {
	config   config.Provider
	logger   logging.Logger
	executor executors.Executor

	service *compute.Service

	osProvisioner osProvisioner
}

func New(cfg globalConfig.Global, logger logging.Logger) (providers.Provider, error) {
	p := &Provider{
		config: cfg.GCP,
		logger: logger,
	}

	osProvisioners := map[string]osProvisioner{
		osWindows: p.provisionWindows,
	}

	provisioner, ok := osProvisioners[cfg.OS]
	if !ok {
		return nil, fmt.Errorf("unsupported OS %q for provisioning", cfg.OS)
	}

	p.osProvisioner = provisioner

	return p, nil
}

func (p *Provider) Get(ctx context.Context, name string) (vm.Instance, error) {
	vmInst := vm.Instance{
		Name: name,
		GCP: config.Instance{
			Project: p.config.Project,
			Zone:    p.config.Zone,
		},
	}

	instance, err := p.getVM(ctx, vmInst.Name)
	if err != nil {
		return vm.Instance{}, fmt.Errorf("couldn't get the instance details: %w", err)
	}

	vmInst.IPAddress = instance.NetworkInterfaces[0].NetworkIP

	err = p.loadCredentialsFromMetadata(ctx, &vmInst, instance)
	if err != nil {
		p.logger.WithError(err).Warn("couldn't load the credentials")
	}

	return vmInst, nil
}

type metadataCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (p *Provider) loadCredentialsFromMetadata(ctx context.Context, vmInst *vm.Instance, instance *compute.Instance) error {
	for _, item := range instance.Metadata.Items {
		if item.Key != vmCredentialsMetadataKey {
			continue
		}

		credentialsBuf := bytes.NewBufferString(*item.Value)

		var credentials metadataCredentials
		err := encoding.
			NewJSON().
			Decode(credentialsBuf, &credentials)
		if err != nil {
			return fmt.Errorf("couldn't parse credentials read from metadata: %w", err)
		}

		vmInst.Username = credentials.Username
		vmInst.Password = credentials.Password

		return nil
	}

	return errors.New("couldn't find credentials in the VM metadata")
}

func (p *Provider) Create(ctx context.Context, e executors.Executor, vmInst *vm.Instance) error {
	log := p.logger.WithField("vm-name", vmInst.Name)
	log.Info("Creating Virtual Machine...")

	specs, err := p.getInstanceSpecs(e, vmInst)
	if err != nil {
		return err
	}

	instances, err := p.getInstancesService(ctx)
	if err != nil {
		return err
	}

	op, err := instances.
		Insert(p.config.Project, p.config.Zone, &specs).
		Context(ctx).
		Do()
	if err != nil {
		return fmt.Errorf("couldn't execute instance create request: %w", err)
	}

	err = p.waitForZoneOperation(ctx, log, "creating-instance", op.Name)
	if err != nil {
		return fmt.Errorf("couldn't get the status of instance create operation: %w", err)
	}

	vmInst.GCP.Project = p.config.Project
	vmInst.GCP.Zone = p.config.Zone

	instance, err := p.getVM(ctx, vmInst.Name)
	if err != nil {
		return fmt.Errorf("couldn't get the instance details: %w", err)
	}

	vmInst.IPAddress = instance.NetworkInterfaces[0].NetworkIP

	if p.osProvisioner != nil {
		err = p.osProvisioner(ctx, log, vmInst, instance)
		if err != nil {
			return fmt.Errorf("couldn't provision the OS: %w", err)
		}
	}

	err = p.saveCredentialsInMetadata(ctx, log, *vmInst)
	if err != nil {
		return fmt.Errorf("couldn't save username and password in the VM metadata: %w", err)
	}

	log.Info("Virtual Machine created")

	return nil
}

func (p *Provider) getInstanceSpecs(e executors.Executor, vmInst *vm.Instance) (compute.Instance, error) {
	provisionScript := e.ProvisionScript()

	region, err := zoneToRegion(p.config.Zone)
	if err != nil {
		return compute.Instance{}, err
	}

	networkInterfaceAccessConfigs := make([]*compute.AccessConfig, 0)
	if !p.config.UseInternalIPOnly {
		networkInterfaceAccessConfigs = append(networkInterfaceAccessConfigs, &compute.AccessConfig{
			Name: "External NAT",
			Type: "ONE_TO_ONE_NAT",
		})
	}

	specs := compute.Instance{
		Name:           vmInst.Name,
		MachineType:    zoneResourceURI(p.config.Project, p.config.Zone, resourcesMachineType, p.config.MachineType),
		MinCpuPlatform: p.config.MinCPUPlatform,
		Metadata: &compute.Metadata{
			Items: []*compute.MetadataItems{
				{
					Key:   startupScript,
					Value: &provisionScript,
				},
				{
					Key:   startupScriptWindows,
					Value: &provisionScript,
				},
			},
		},
		Tags: &compute.Tags{
			Items: p.config.Tags,
		},
		Disks: []*compute.AttachedDisk{
			{
				Boot:       true,
				AutoDelete: true,
				Type:       "PERSISTENT",
				Mode:       "READ_WRITE",
				InitializeParams: &compute.AttachedDiskInitializeParams{
					DiskName:    vmInst.Name + "-disk",
					SourceImage: p.config.Image,
					DiskType:    zoneResourceURI(p.config.Project, p.config.Zone, resourcesDiskType, p.config.DiskType),
					DiskSizeGb:  p.config.DiskSize,
				},
			},
		},
		NetworkInterfaces: []*compute.NetworkInterface{
			{
				Network:       globalResourceURI(p.config.Project, resourceNetwork, p.config.Network),
				Subnetwork:    regionResourceURI(p.config.Project, region, resourceSubnetwork, p.config.Subnetwork),
				AccessConfigs: networkInterfaceAccessConfigs,
			},
		},
	}

	return specs, nil
}

func (p *Provider) getInstancesService(ctx context.Context) (*compute.InstancesService, error) {
	s, err := p.getService(ctx)
	if err != nil {
		return nil, fmt.Errorf("couldn't get instances service: %w", err)
	}

	return s.Instances, nil
}

func (p *Provider) getService(ctx context.Context) (*compute.Service, error) {
	if p.service != nil {
		return p.service, nil
	}

	service, err := compute.NewService(ctx, option.WithCredentialsFile(p.config.ServiceAccountFile))
	if err != nil {
		return nil, fmt.Errorf("couldn't create GCP Compute service: %w", err)
	}

	service.UserAgent = autoscaler.Version().UserAgent()

	p.service = service

	return service, nil
}

func (p *Provider) waitForZoneOperation(ctx context.Context, logger logging.Logger, operation string, name string) error {
	s, err := p.getZoneOperationsService(ctx)
	if err != nil {
		return err
	}

	return p.waitForOperation(logger, operation, name, func() (*compute.Operation, error) {
		return s.Get(p.config.Project, p.config.Zone, name).Do()
	})
}

func (p *Provider) getZoneOperationsService(ctx context.Context) (*compute.ZoneOperationsService, error) {
	s, err := p.getService(ctx)
	if err != nil {
		return nil, err
	}

	return s.ZoneOperations, nil
}

func (p *Provider) waitForOperation(logger logging.Logger, operation string, name string, opChecker func() (*compute.Operation, error)) error {
	log := logger.WithFields(logging.Fields{
		"operation":      operation,
		"operation-name": name,
	})

	b := backoff.Settings{
		InitialInterval:     3,
		RandomizationFactor: 0.1,
		Multiplier:          1.5,
		MaxInterval:         30,
		MaxElapsedTime:      300,
	}
	err := backoff.Loop(b, log, func(logger logging.Logger) (bool, error) {
		logger.Info("Waiting for operation result...")

		op, err := opChecker()
		if err != nil {
			return false, fmt.Errorf("couldn't get the status of operation %q: %w", name, err)
		}

		logger.
			WithField("operation-status", op.Status).
			Info("Got operation status")

		if op.Error != nil {
			return false, fmt.Errorf("operation %q error: %#v", name, op.Error.Errors[0])
		}

		if op.Status != operationStateDone {
			return false, nil
		}

		return true, nil
	})

	return err
}

func (p *Provider) getVM(ctx context.Context, name string) (*compute.Instance, error) {
	s, err := p.getInstancesService(ctx)
	if err != nil {
		return nil, err
	}

	return s.Get(p.config.Project, p.config.Zone, name).Do()
}

func (p *Provider) saveCredentialsInMetadata(ctx context.Context, logger logging.Logger, vmInst vm.Instance) error {
	instance, err := p.getVM(ctx, vmInst.Name)
	if err != nil {
		return fmt.Errorf("couldn't get the instance details: %w", err)
	}

	credentials := metadataCredentials{
		Username: vmInst.Username,
		Password: vmInst.Password,
	}

	credentialsBuf := new(bytes.Buffer)
	err = encoding.
		NewJSON().
		Encode(credentials, credentialsBuf)
	if err != nil {
		return fmt.Errorf("couldn't transform credentials to JSON: %w", err)
	}

	credentialsJSON := credentialsBuf.String()

	metadata := instance.Metadata
	found := false
	for _, item := range metadata.Items {
		if item.Key != vmCredentialsMetadataKey {
			continue
		}

		item.Value = &credentialsJSON
		found = true

		break
	}

	if !found {
		metadata.Items = append(metadata.Items, &compute.MetadataItems{
			Key:   vmCredentialsMetadataKey,
			Value: &credentialsJSON,
		})
	}

	return p.updateMetadata(ctx, logger, vmInst, metadata)
}

func (p *Provider) updateMetadata(ctx context.Context, logger logging.Logger, vmInst vm.Instance, metadata *compute.Metadata) error {
	logger.Info("Updating Virtual Machine metadata...")

	instances, err := p.getInstancesService(ctx)
	if err != nil {
		return err
	}

	metaOp, err := instances.
		SetMetadata(p.config.Project, p.config.Zone, vmInst.Name, metadata).
		Context(ctx).
		Do()
	if err != nil {
		return fmt.Errorf("couldn't execute instance set metadata request: %w", err)
	}

	err = p.waitForZoneOperation(ctx, logger, "set-metadata", metaOp.Name)
	if err != nil {
		return fmt.Errorf("couldn't get the status of instance set metadata operation: %w", err)
	}

	logger.Info("Virtual Machine metadata updated")

	return nil
}

func (p *Provider) Delete(ctx context.Context, vmInst vm.Instance) error {
	log := p.logger.WithField("vm-name", vmInst.Name)
	log.Info("Deleting Virtual Machine...")

	instances, err := p.getInstancesService(ctx)
	if err != nil {
		return err
	}

	op, err := instances.Delete(vmInst.GCP.Project, vmInst.GCP.Zone, vmInst.Name).Context(ctx).Do()
	if err != nil {
		return fmt.Errorf("couldn't execute instance delete request: %w", err)
	}

	err = p.waitForZoneOperation(ctx, log, "delete-instance", op.Name)
	if err != nil {
		return fmt.Errorf("couldn't get the status of instance delete operation: %w", err)
	}

	log.Info("Virtual Machine deleted")

	return nil
}
