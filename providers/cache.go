package providers

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/encoding"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/fs"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const (
	cacheFileMode os.FileMode = 0600
)

type CacheStore interface {
	Read(vmName string) (vm.Instance, error)
	Write(vmInst vm.Instance) error
	Delete(vmInst vm.Instance) error
}

type fsCacheStore struct {
	directory string
	fs        fs.FS
	encoder   encoding.Encoder
}

func newFSCacheStorage(directory string, f fs.FS, e encoding.Encoder) CacheStore {
	return &fsCacheStore{
		directory: directory,
		fs:        f,
		encoder:   e,
	}
}

func (d *fsCacheStore) Read(vmName string) (vm.Instance, error) {
	var newVM vm.Instance

	path := d.vmFilePath(vmName)

	exists, err := d.fs.Exists(path)
	if err != nil {
		return newVM, fmt.Errorf("can't access cache file: %w", err)
	}

	if !exists {
		return newVM, os.ErrNotExist
	}

	data, err := d.fs.ReadFile(path)
	if err != nil {
		return newVM, err
	}

	err = d.encoder.Decode(bytes.NewBuffer(data), &newVM)
	if err != nil {
		return newVM, err
	}

	return newVM, nil
}

func (d *fsCacheStore) vmFilePath(vmName string) string {
	return filepath.Join(d.directory, fmt.Sprintf("%s.json", vmName))
}

func (d *fsCacheStore) Write(vmInst vm.Instance) error {
	buf := new(bytes.Buffer)

	err := d.encoder.Encode(vmInst, buf)
	if err != nil {
		return err
	}

	return d.fs.WriteFile(d.vmFilePath(vmInst.Name), buf.Bytes(), cacheFileMode)
}

func (d *fsCacheStore) Delete(vmInst vm.Instance) error {
	return d.fs.Remove(d.vmFilePath(vmInst.Name))
}

type cacheProviderDecorator struct {
	logger   logging.Logger
	internal Provider
	store    CacheStore
}

func newCacheProviderDecorator(cfg config.Global, logger logging.Logger, internal Provider) Provider {
	cacheConfig := cfg.ProviderCache

	if cacheConfig == nil || !cacheConfig.Enabled {
		return internal
	}

	return &cacheProviderDecorator{
		logger:   logger,
		internal: internal,
		store:    newFSCacheStorage(cacheConfig.Directory, fs.NewOS(), encoding.NewJSON()),
	}
}

func (c *cacheProviderDecorator) Get(ctx context.Context, vmName string) (vm.Instance, error) {
	vmInst, err := c.store.Read(vmName)
	if err == nil {
		return vmInst, nil
	}

	log := c.logger.WithField("vm-name", vmName)

	readLog := log.WithError(err)
	writeReadLog := readLog.Warning
	if errors.Is(err, os.ErrNotExist) {
		writeReadLog = readLog.Debug
	}

	writeReadLog("Error while trying to read cache of the VM metadata")

	vmInst, err = c.internal.Get(ctx, vmName)
	if err != nil {
		return vm.Instance{}, err
	}

	err = c.store.Write(vmInst)
	if err != nil {
		log.WithError(err).
			Error("Error while trying to write cache of the VM metadata")
	}

	return vmInst, nil
}

func (c *cacheProviderDecorator) Create(ctx context.Context, e executors.Executor, vmInst *vm.Instance) error {
	err := c.internal.Create(ctx, e, vmInst)
	if err != nil {
		return err
	}

	err = c.store.Write(*vmInst)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("vm-name", vmInst.Name).
			Error("Error while trying to write cache of the VM metadata")
	}

	return nil
}

func (c *cacheProviderDecorator) Delete(ctx context.Context, vmInst vm.Instance) error {
	err := c.internal.Delete(ctx, vmInst)
	if err != nil {
		return err
	}

	err = c.store.Delete(vmInst)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("vm-name", vmInst.Name).
			Error("Error while trying to write cache of the VM metadata")
	}

	return nil
}
